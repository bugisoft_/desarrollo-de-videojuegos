import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class GamePanel extends JPanel implements Runnable {
	private static final long serialVersionUID = 1L;
	
	private Thread animator;
	private Graphics dbg;
	private Image dbImage = null;

	private GameStateContext context;
	
	//Constantes para la medida de la pantalla
	private static final int PWIDTH = 1200;
	private static final int PHEIGHT = 750;
	
	public GamePanel(){
		setBackground(Color.white);
		setPreferredSize(new Dimension(PWIDTH,PHEIGHT));
		setFocusable(true);
		requestFocus();
		readyForTermination();
		
		context = new GameStateContext();
		addKeyListener( new KeyAdapter() { 
			public void keyPressed(KeyEvent e) {
				context.keyPressed(e);
			}
			
			public void keyReleased(KeyEvent e) {
				context.keyReleased(e);
			}
		});
		addMouseListener( new MouseAdapter() { 
			public void mousePressed(MouseEvent e) {
				context.mousePressed(e);
			}
		});
	}
	
	public void addNotify()
	{
		super.addNotify();
		startGame();
	}
	
	private void startGame()
	{
		if(animator == null || !context.isRunning()){
			animator = new Thread(this);
			animator.start();
		}
	}
	
	public void stopGame(){
		context.setRunning(false);
	}
	
	public void run(){
		context.setRunning(true);
		while(context.isRunning()){
			gameUpdate();
			gameRender();
			paintScreen();
			try{
				Thread.sleep(1000/60);
			}catch(InterruptedException ex){}
		}
		System.exit(0);
	}
	
	private void gameUpdate(){
		context.update();
	}
	
	private void gameRender(){
		if(dbImage == null){
			dbImage = createImage(PWIDTH,PHEIGHT);
			if(dbImage == null){
				System.out.println("dbImage is null");
				return;
			}else{
				dbg = dbImage.getGraphics();
			}
		}
		dbg.setColor(Color.white);
		dbg.fillRect(0,0,PWIDTH,PHEIGHT);
		context.render(dbg);
	}
	
	public void paintComponent(Graphics g){
		super.paintComponent(g);
		if(dbImage != null)
			g.drawImage(dbImage, 0, 0, null);
	}
	
	private void readyForTermination() {
		addKeyListener( new KeyAdapter() { 
			public void keyPressed(KeyEvent e) { 
				int keyCode = e.getKeyCode();
				if ((keyCode == KeyEvent.VK_ESCAPE) ||
					(keyCode == KeyEvent.VK_Q) ||
					(keyCode == KeyEvent.VK_END) ||
					((keyCode == KeyEvent.VK_C) && e.isControlDown()) ) {
					context.setRunning(false);
				}				
			} 
		});
	}

	private void paintScreen(){
		Graphics g;
		try{
			g = this.getGraphics();
			if((g != null) && (dbImage != null))
				g.drawImage(dbImage,0,0,null);
			Toolkit.getDefaultToolkit().sync();
		}
		catch(Exception e){
			System.out.println("Graphics context error: " + e);
		}
	}
	
	public static void main(String args[]){
		ImageLoader im = new ImageLoader();
		JFrame app = new JFrame("The BloodThirster");
	    app.getContentPane().add(new GamePanel(), BorderLayout.CENTER);
	    app.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    app.setUndecorated(true);
	    app.pack();
	    app.setLocationRelativeTo(null);
	    app.setResizable(false);  
	    app.setVisible(true);
	}
}
import java.awt.Color;

//Corre y ataca mucho mas rapido que el jugador pero tiene poca vida y muy debil ataque
public class EnemigoRunner extends Enemigo{
	//Espera entre ataques.
	private static final int DELAY = 10;
	//Velocidad de movimiento
	private static final int VELOCIDAD = 15;
	//Golpe del Enemigo
	private static final int GOLPE = 4;
	//Nombre de la imagen
	private static final String NAME = "Runner_";
	
	public EnemigoRunner() {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),9,NAME);
	}
	
	public EnemigoRunner(int pos) {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
	
	public EnemigoRunner(int x, int y, int vida, int delay, int pos) {
		super(x,y,vida,delay,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
}
/*
 * GameStateRunning.java
 * 
 * Copyright 2017 Edgar Daniel Fernández Rodríguez <edgar.fernandez@me.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
public class GameStateRunning implements GameState {
	
	GameStateContext gc;
	
	public GameStateRunning(GameStateContext gc) {
		this.gc = gc;
	}
	
	public GameStateRunning() {

	}
	
	public void setContext(GameStateContext context) {
		gc = context;
	}
	
	public void end(){
		gc.setCurrent(gc.getGameOver());
	}
	
	public void pause(){
		gc.setCurrent(gc.getPause());
	}
	
	public void resume(){
		
	}
	
	public void render(Graphics g){
		g.drawImage(gc.getBackground(), 0, 0, null);
		gc.getCharacterController().interaccionPersonajes();
		gc.getCharacterController().pintate(g);
	}
	
	public void update(){
		if(gc.getCharacterController().gameOver()) {
			end();
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int keyCode = e.getKeyCode();
		gc.getCharacterController().moverJugador(keyCode);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		int keyCode = e.getKeyCode();
		gc.getCharacterController().detenerJugador(keyCode);
	}

	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void menu() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub
		if(e.getX() >= 1120 && e.getX() <= 1185 && e.getY() >= 670 && e.getY() <= 735) {
			pause();
		}
	}
}
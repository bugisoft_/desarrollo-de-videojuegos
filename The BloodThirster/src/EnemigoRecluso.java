import java.awt.Color;

//Corre y ataca mas rapido que el jugador pero tiene poca vida y debil ataque
public class EnemigoRecluso extends Enemigo{
	//Espera entre ataques.
	private static final int DELAY = 15;
	//Velocidad de movimiento
	private static final int VELOCIDAD = 12;
	//Golpe del Enemigo
	private static final int GOLPE = 5;
	//Nombre de la imagen
	private static final String NAME = "Recluso_";
	
	public EnemigoRecluso() {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),9,NAME);
	}
	
	public EnemigoRecluso(int pos) {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
	
	public EnemigoRecluso(int x, int y, int vida, int delay, int pos) {
		super(x,y,vida,delay,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
}
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.imageio.ImageIO;

public class ImageLoader {
	private final static String DIR = "media/";
	private HashMap images;
	private GraphicsConfiguration config;
	
	private static ImageLoader instance;
	
	public ImageLoader(){
		images = new HashMap();
		
		loadSingleImage("Ribbon.png");
		loadSingleImage("TheBloodThirster.png");
		loadSingleImage("Background.png");
		loadSingleImage("Instrucciones.png");
		loadSingleImage("Pause1.png");
		loadSingleImage("GameOver.png");
		loadSingleImage("Enemigo_0.png");
		loadSingleImage("Enemigo_1.png");
		loadSingleImage("Enemigo_2.png");
		loadSingleImage("Enemigo_3.png");
		loadSingleImage("Heavy_0.png");
		loadSingleImage("Heavy_1.png");
		loadSingleImage("Heavy_2.png");
		loadSingleImage("Heavy_3.png");
		loadSingleImage("Personaje_0.png");
		loadSingleImage("Personaje_1.png");
		loadSingleImage("Personaje_2.png");
		loadSingleImage("Personaje_3.png");
		loadSingleImage("Poderoso_0.png");
		loadSingleImage("Poderoso_1.png");
		loadSingleImage("Poderoso_2.png");
		loadSingleImage("Poderoso_3.png");
		loadSingleImage("Recluso_0.png");
		loadSingleImage("Recluso_1.png");
		loadSingleImage("Recluso_2.png");
		loadSingleImage("Recluso_3.png");
		loadSingleImage("Runner_0.png");
		loadSingleImage("Runner_1.png");
		loadSingleImage("Runner_2.png");
		loadSingleImage("Runner_3.png");
		
		GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		config = ge.getDefaultScreenDevice().getDefaultConfiguration();
	}
	
	public static ImageLoader getInstance(){
		if(instance == null){
			instance = new ImageLoader();
		}
		return instance;
	}
	
	public boolean loadSingleImage(String fnm){
		String name = getPrefix(fnm);
		if(images.containsKey(name)){
			System.out.println("Error: " + name + " already used");
			return false;
		}
		
		BufferedImage bi = loadImage(fnm);
		if(bi != null){
			ArrayList imsList = new ArrayList();
			imsList.add(bi);
			images.put(name, imsList);
			return true;
		}else
			return false;
		
	}
	
	private String getPrefix(String fnm){
		int posn;
		if((posn = fnm.lastIndexOf(".")) == -1){
			System.err.println("No prefix found for " + fnm);
			return fnm;
		}else{
			return fnm.substring(0, posn);
		}
	}
	
	private BufferedImage loadImage(String fileName){
		try{
			BufferedImage image = ImageIO.read(new File(DIR+fileName));
			Graphics2D g2d = image.createGraphics();
			g2d.drawImage(image, 0, 0, null);
			g2d.dispose();
			return image;
		}catch(IOException e){
			return null;
		}
	}
	
	public BufferedImage getImage(String name){
		ArrayList imsList = (ArrayList) images.get(name);
		if(imsList == null){
			System.out.println("No images stored under " + name);
			return null;
		}
		
		return (BufferedImage) imsList.get(0);
	}
}

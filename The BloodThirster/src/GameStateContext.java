/*
 * GameStateContext.java
 * 
 * Copyright 2017 Edgar Daniel Fernández Rodríguez <edgar.fernandez@me.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;

public class GameStateContext {
	
	private GameState run;
	private GameState pause;
	private GameState go;
	private GameState menu;
	private GameState current;
	
	private boolean running;
	private boolean gameOver;
	private boolean isPaused;
	private boolean mute;
	private boolean instrucciones;
	
	private Ribbon r;
	
	private CharacterController characterController;
	protected BufferedImage background;
	protected BufferedImage ribbon;
	protected BufferedImage pauseImage;
	protected BufferedImage gameOverImage;
	protected BufferedImage instruccionesImage;
	
	public GameStateContext(){
		setCharacterController(new CharacterController());
		
		background = ImageLoader.getInstance().getImage("Background");
		ribbon = ImageLoader.getInstance().getImage("TheBloodThirster");
		pauseImage = ImageLoader.getInstance().getImage("Pause1");
		gameOverImage = ImageLoader.getInstance().getImage("GameOver");
		instruccionesImage = ImageLoader.getInstance().getImage("Instrucciones");
		
		run = StatesFactory.getInstance().getState("Running");
		pause = StatesFactory.getInstance().getState("Paused");
		go = StatesFactory.getInstance().getState("GameOver");
		menu = StatesFactory.getInstance().getState("Menu");
		
		run.setContext(this);
		pause.setContext(this);
		go.setContext(this);
		menu.setContext(this);
		
		mute = false;
		instrucciones = false;
		
		this.r = new Ribbon(1200,750,ribbon,1);
		r.moveLeft();

		current = menu;
	}
	
	public GameState getRun(){
		return run;
	}
	
	public GameState getPause(){
		return pause;
	}
	
	public GameState getGameOver(){
		return go;
	}
	
	public GameState getMenu(){
		return menu;
	}
	
	public boolean isRunning() {
		return running;
	}

	public void setRunning(boolean running) {
		this.running = running;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}
	
	public boolean isPaused() {
		return isPaused;
	}

	public void setIsPaused(boolean isPaused) {
		this.isPaused = isPaused;
	}
	
	public CharacterController getCharacterController() {
		return characterController;
	}

	public void setCharacterController(CharacterController characterController) {
		this.characterController = characterController;
	}
	
	public void end(){
		current.end();
	}
	
	public void run(){
		current.run();
	}
	
	public void pause(){
		current.pause();
	}
	
	public void setCurrent(GameState gs){
		current = gs;
	}
	
	public void update(){
		current.update();
	}
	
	public void render(Graphics g){
		current.render(g);
	}
	
	public void keyPressed(KeyEvent e) {
		if(!gameOver && !isPaused && running) {
			current.keyPressed(e);
		}
	}

	public void keyReleased(KeyEvent e) {
		if(!gameOver && !isPaused && running) {
			current.keyReleased(e);
		}
	}
	
	public Ribbon getRibbon() {
		return r;
	}
	
	public BufferedImage getBackground() {
		return background;
	}
	
	public BufferedImage getPauseImage() {
		return pauseImage;
	}
	
	public BufferedImage getGameOverImage() {
		return gameOverImage;
	}
	
	public BufferedImage getInstruccionesImage() {
		return instruccionesImage;
	}

	public void mousePressed(MouseEvent e) {
		current.mousePressed(e);
	}

	public boolean isMute() {
		return mute;
	}

	public void setMute(boolean mute) {
		this.mute = mute;
	}

	public boolean isInstrucciones() {
		return instrucciones;
	}

	public void setInstrucciones(boolean instrucciones) {
		this.instrucciones = instrucciones;
	}
}
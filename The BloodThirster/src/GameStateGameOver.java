/*
 * GameStateGameOver.java
 * 
 * Copyright 2017 Edgar Daniel Fernández Rodríguez <edgar.fernandez@me.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class GameStateGameOver implements GameState {
	GameStateContext gc;
	
	public GameStateGameOver(GameStateContext gc){
		this.gc = gc;
	}
	
	public GameStateGameOver(){

	}
	
	public void setContext(GameStateContext context) {
		gc = context;
	}
	
	public void end(){
		
	}
	
	public void pause(){
		
	}
	
	public void run(){
		
	}
	
	public void menu() {
		gc.setCurrent(gc.getMenu());
	}
	
	public void render(Graphics g){
		g.drawImage(gc.getBackground(), 0, 0, null);
		gc.getCharacterController().pintate(g);
		g.drawImage(gc.getGameOverImage(), 80, 50, null);
		g.setColor(new Color(255,255,255));
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 75));
		g.drawString("The BloodThirster", 270, 150);
		g.drawString("Tu puntaje fue: " + gc.getCharacterController().getJugador().getScore(), 220, 630);
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 55));
		g.drawString("Volver al Menu Principal", 237, 310);
		g.drawString("Dejar de Jugar", 380, 460);
	}
	
	public void update(){
		gc.setGameOver(true);
	}

	@Override
	public void keyPressed(KeyEvent e) {
		
	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void start() {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(e.getX() >= 235 && e.getX() <= 965 && e.getY() >= 270 && e.getY() <= 340) {
			gc.getCharacterController().setDefault();
			gc.setGameOver(true);
			menu();
		}else if(e.getX() >= 376 && e.getX() <= 800 && e.getY() >= 415 && e.getY() <= 490) {
			gc.setRunning(false);
		}
	}
}
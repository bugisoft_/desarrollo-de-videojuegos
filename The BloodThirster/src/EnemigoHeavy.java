import java.awt.Color;

//Gran resistencia y ataques fuertes pero golpea y camina lento.
public class EnemigoHeavy extends Enemigo{
	//Espera entre ataques.
	private static final int DELAY = 50;
	//Velocidad de movimiento
	private static final int VELOCIDAD = 2;
	//Golpe del Enemigo
	private static final int GOLPE = 18;
	//Nombre de la imagen
	private static final String NAME = "Heavy_";
	
	public EnemigoHeavy() {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),9,NAME);
	}
	
	public EnemigoHeavy(int pos) {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
	
	public EnemigoHeavy(int x, int y, int vida, int delay, int pos) {
		super(x,y,vida,delay,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
}

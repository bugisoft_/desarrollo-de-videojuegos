import java.awt.Color;

public abstract class Enemigo extends Personaje{
	//Constantes
	//Tama�o
	private static final int SIZE = 20;
	private static final int MOVENEMIGODEFAULT = 2;
	//Golpe
	private static final int GOLPEENEMIGODEFAULT = 10;
	//Posicion de inicio.
	//Valores fijos de paredes.
	private static final int YSUPERIOR = 5;
	private static final int YINFERIOR = 745;
	private static final int XIZQUIERDA = 5;
	private static final int XDERECHA = 1195;
	//Valores distintospara cada entrada
	private static final int XSUPERIOR1 = 168;
	private static final int XSUPERIOR2 = 532;
	private static final int XSUPERIOR3 = 912;
	private static final int XINFERIOR1 = 363;
	private static final int XINFERIOR2 = 763;
	private static final int YIZQUIERDA1 = 130;
	private static final int YIZQUIERDA2 = 460;
	private static final int YDERECHA1 = 288;
	private static final int YDERECHA2 = 620;
	
	private int delay;
	
	public Enemigo() {
		super(50,50,50,SIZE,MOVENEMIGODEFAULT,GOLPEENEMIGODEFAULT,new Color(255,0,0));
		
		for(int i=0;i<4;i++){
			String img = new StringBuilder("Enemigo_").append(i).toString();
			this.buffers[i] = ImageLoader.getInstance().getImage(img);
		}
		
		this.buffered = this.buffers[1];
		this.rotationRequired = 0;
		this.animation = 0;
		this.setSize(buffers[0].getWidth()-2);
		this.locationX = buffers[0].getWidth() / 2;
		this.locationY = buffers[0].getHeight() / 2;
	}
	
	public Enemigo(int x, int y, int vida, int delay, int vel, int golpe, Color color, int pos, String name) {
		super(x, y, vida, SIZE, vel, golpe, color);
		
		switch(pos) {
		case 0:
			this.setPx(XSUPERIOR1);
			this.setPy(YSUPERIOR);
			break;
		case 1:
			this.setPx(XSUPERIOR2);
			this.setPy(YSUPERIOR);
			break;
		case 2:
			this.setPx(XSUPERIOR3);
			this.setPy(YSUPERIOR);
			break;
		case 3:
			this.setPx(XDERECHA - this.getSize());
			this.setPy(YDERECHA1);
			break;
		case 4:
			this.setPx(XDERECHA - this.getSize());
			this.setPy(YDERECHA2);
			break;
		case 5:
			this.setPx(XINFERIOR2);
			this.setPy(YINFERIOR - this.getSize());
			break;
		case 6:
			this.setPx(XINFERIOR1);
			this.setPy(YINFERIOR - this.getSize());
			break;
		case 7:
			this.setPx(XIZQUIERDA);
			this.setPy(YIZQUIERDA2);
			break;
		case 8:
			this.setPx(XIZQUIERDA);
			this.setPy(YIZQUIERDA1);
			break;
		default:
			this.setPx(50);
			this.setPy(50);
		}
		
		for(int i=0;i<4;i++){
			String img = new StringBuilder(name).append(i).toString();
			this.buffers[i] = ImageLoader.getInstance().getImage(img);
		}
		
		this.setSize(buffers[0].getWidth()-2);
		this.locationX = buffers[0].getWidth() / 2;
		this.locationY = buffers[0].getHeight() / 2;
	}
	
	public void delayAtaque() {
		this.setCont(delay);
		this.setAtacar(false);
	}
}
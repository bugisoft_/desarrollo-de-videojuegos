import java.awt.Color;

//Gran resistencia, ataques fuertes, se mueve y golpea rapido.
public class EnemigoPoderoso extends Enemigo{
	//Espera entre ataques.
	private static final int DELAY = 25;
	//Velocidad de movimiento
	private static final int VELOCIDAD = 9;
	//Golpe del Enemigo
	private static final int GOLPE = 12;
	//Nombre de la imagen
	private static final String NAME = "Poderoso_";
	
	public EnemigoPoderoso() {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),9,NAME);
	}
	
	public EnemigoPoderoso(int pos) {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
	
	public EnemigoPoderoso(int x, int y, int vida, int delay, int pos) {
		super(x,y,vida,delay,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
}

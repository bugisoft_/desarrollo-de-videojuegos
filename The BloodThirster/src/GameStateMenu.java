/*
 * GameStateRunning.java
 * 
 * Copyright 2017 Edgar Daniel Fernández Rodríguez <edgar.fernandez@me.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
public class GameStateMenu implements GameState {
	
	GameStateContext gc;
	
	public GameStateMenu(GameStateContext gc) {
		this.gc = gc;
	}
	
	public GameStateMenu() {
		
	}
	
	public void setContext(GameStateContext context) {
		gc = context;
	}
	
	public void end(){
		
	}
	
	public void pause(){
		
	}
	
	public void resume(int dif){
		gc.setGameOver(false);
		gc.setRunning(true);
		gc.getCharacterController().setDificultad(dif);
		gc.setCurrent(gc.getRun());
	}
	
	public void render(Graphics g){
		gc.getRibbon().update();
		gc.getRibbon().paint(g);
		g.setColor(new Color(255,255,255));
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 75));
		g.drawString("The BloodThirster", 300, 100);
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 40));
		g.drawString("Elige una dificultad:", 400, 230);
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 45));
		g.drawString("Sencishito", 478, 350);
		g.drawString("Para Mortales", 438, 470);
		g.drawString("Mago Roberto", 457, 590);
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 30));
		g.drawString("Sonido", 30, 50);
		g.drawString("Dejar de Jugar", 30, 720);
		g.drawString("Instrucciones", 960, 720);
		if(gc.isMute()) {
			g.drawLine(30, 60, 140, 30);
			g.drawLine(30, 30, 140, 60);
		}
		if(gc.isInstrucciones()) {
			g.drawImage(gc.getInstruccionesImage(), 120, 75, null);
		}
	}
	
	public void update(){
		
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {

	}

	@Override
	public void start() {
		
	}

	@Override
	public void run() {
		
	}

	@Override
	public void menu() {
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(!gc.isInstrucciones()) {
			if(e.getX() >= 475 && e.getX() <= 715 && e.getY() >= 315 && e.getY() <= 355) {
				if(!gc.isMute()) {
					SoundLoader.getInstance().playSound("sencishito");
				}
				resume(1);
			}else if(e.getX() >= 430 && e.getX() <= 765 && e.getY() >= 435 && e.getY() <= 485) {
				if(!gc.isMute()) {
					SoundLoader.getInstance().playSound("letsplay");
				}
				resume(2);
			}else if(e.getX() >= 450 && e.getX() <= 750 && e.getY() >= 555 && e.getY() <= 615) {
				if(!gc.isMute()) {
					SoundLoader.getInstance().playSound("magoroberto");
				}
				resume(3);
			}else if(e.getX() >= 30 && e.getX() <= 140 && e.getY() >= 30 && e.getY() <= 60) {
				gc.setMute(!gc.isMute());
				if(gc.isMute()) SoundLoader.getInstance().stopMusic();
				else SoundLoader.getInstance().startMusic();
			}else if(e.getX() >= 960 && e.getX() <= 1175 && e.getY() >= 695 && e.getY() <= 730) {
				gc.setInstrucciones(true);
			}else if(e.getX() >= 30 && e.getX() <= 265 && e.getY() >= 695 && e.getY() <= 740) {
				gc.setRunning(false);
			}
		}else if(e.getX() >= 930 && e.getX() <= 1065 && e.getY() >= 640 && e.getY() <= 665){
			gc.setInstrucciones(false);
		}
	}
}
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Random;

public class CharacterController {
	//Constantes
	//Robo de vida de personajes.
	private static final int LIFESTEALJUGADOR = 80;
	private static final int LIFESTEALENEMIGO = 75;
	// Vida M�xima
	private static final int MAXVIDA = 300;
	
	//Contadores
	//Crear enemigos y disminuir vida.
	private int cont;
	
	private LinkedList<Enemigo> enemigos;
	private LinkedList<Enemigo> copy;
	private Jugador jugador;
	
	private int pos = 0;
	private int pasado = -1;
	private int dificultad;
	Random random;
	
	public CharacterController() {
		cont = 0;
		random = new Random();
		jugador = new Jugador();
		dificultad = 1;
		
		enemigos = new LinkedList<Enemigo>();
		enemigos.add(EnemigosFactory.getInstance().makeEnemigos(valorEnemigo(jugador.getVida()), random.nextInt(4)));
		enemigos.add(EnemigosFactory.getInstance().makeEnemigos(valorEnemigo(jugador.getVida()), 4 + random.nextInt(4)));
	}
	
	//Metodos del jugador
	public void moverJugador(int keyCode) {
		if (keyCode == KeyEvent.VK_UP) {
			jugador.setVy(jugador.getVelocidad()*(-1));
		}
		if (keyCode == KeyEvent.VK_LEFT) {
			jugador.setVx(jugador.getVelocidad()*(-1));
		}
		if (keyCode == KeyEvent.VK_DOWN) {
			jugador.setVy(jugador.getVelocidad());
		}
		if (keyCode == KeyEvent.VK_RIGHT) {
			jugador.setVx(jugador.getVelocidad());
		}
		if (keyCode == KeyEvent.VK_A) {
			if(jugador.isAtacar()){
				jugadorAtaca();
				jugador.delayAtaque();
			}
		}
	}
	
	public void detenerJugador(int keyCode) {
		if (keyCode == KeyEvent.VK_LEFT || keyCode == KeyEvent.VK_RIGHT){
			jugador.setVx(0);
		}
		if (keyCode == KeyEvent.VK_UP || keyCode == KeyEvent.VK_DOWN) {
			jugador.setVy(0);
		}
	}
	
	public void disminuyeVida() {
		jugador.setVida(jugador.getVida() - 1);
	}
	
	public void jugadorAtaca() {
		Iterator<Enemigo> iterator = enemigos.iterator();
		while(iterator.hasNext()) {
			Enemigo e = iterator.next();
			int x = (e.getPx() + e.getSize()/2) - (jugador.getPx() + jugador.getSize()/2);
			int y = (e.getPy() + e.getSize()/2) - (jugador.getPy() + jugador.getSize()/2);
			if (Math.sqrt(x*x + y*y) <= jugador.getSize()*(2)) {
				e.setVida(e.getVida() - jugador.getGolpe());
				int lifeSteal = (int) (Math.floor(jugador.getGolpe()*LIFESTEALJUGADOR*(1.1 - (float)jugador.getVida()/MAXVIDA)/100));
				if(jugador.getVida() + lifeSteal > MAXVIDA) {
					jugador.setVida(MAXVIDA);
				}else {
					jugador.setVida(jugador.getVida() + (int) Math.floor(lifeSteal) - dificultad);
				}
			}
			if(e.getVida() < 1) {
				iterator.remove();
			}
		}
	}
	
	//Metodos de los enemigos	
	public void enemigosAtacan() {
		for(Enemigo e: enemigos) {
			if(e.isAtacar()) {
				int x = (e.getPx() + e.getSize()/2) - (jugador.getPx() + jugador.getSize()/2);
				int y = (e.getPy() + e.getSize()/2) - (jugador.getPy() + jugador.getSize()/2);
				if (Math.sqrt(x*x + y*y) <= e.getSize()*(2)) {
					float lifeSteal = (float) (e.getGolpe()*LIFESTEALENEMIGO*(0.3 + (float)jugador.getVida()/MAXVIDA)/100);
					if(jugador.getVida() - e.getGolpe() < 0) {
						jugador.setVida(0);
					}else {
						e.setVida(e.getVida() + (int) Math.ceil(lifeSteal));
						jugador.setVida(jugador.getVida() - e.getGolpe() - dificultad);
					}
					e.delayAtaque();
				}
			}else if(e.getCont() > 0){
				e.disminuirCont();
			}else {
				e.setAtacar(true);
			}
		}
	}
	
	public boolean colisionEnemigoPersonaje(Enemigo e) {
		Rectangle player = new Rectangle(jugador.getPx(), jugador.getPy(), (int) (jugador.getSize() + e.getVelocidad()), (int) (jugador.getSize() + e.getVelocidad()));
		Rectangle enemigo = new Rectangle(e.getPx(), e.getPy(), (int) (e.getSize() + (2)*e.getVelocidad()), (int) (e.getSize() + (2)*e.getVelocidad()));
		return !(player.intersects(enemigo));
	}
	
	public void direccionEnemigo() {
		for (Enemigo e: enemigos) {
			if(colisionEnemigoPersonaje(e)) {
				if(e.getPx() != jugador.getPx()) {
					e.setVx(e.getPx() < jugador.getPx() ? e.getVelocidad():(e.getVelocidad()*-1));
					if(Math.abs(e.getPx() - jugador.getPx()) <= e.getVelocidad()){
						e.setVx(e.getPx() < jugador.getPx() ? 1:-1);
					}
				}else e.setVx(0);
				if(e.getPy() != jugador.getPy()) {
					e.setVy(e.getPy() < jugador.getPy() ? e.getVelocidad():(e.getVelocidad()*-1));
					if(Math.abs(e.getPy() - jugador.getPy()) <= e.getVelocidad()){
						e.setVy(e.getPy() < jugador.getPy() ? 1:-1);
					}
				}else e.setVy(0);
				e.moverPersonaje(enemigos, e.getVelocidad());
			}
		}
	}
	
	public boolean gameOver() {
		if(jugador.getVida() < 1) {
			return true;
		}
		return false;
	}
	
	public void interaccionPersonajes() {
		if(cont == 4000) {
			dificultad++;
			cont = 1;
		}else {
			cont++;
		}
		
		if(cont % 10 == 0) {
			direccionEnemigo();
			enemigosAtacan();
		}
		
		if(jugador.getCont() > 0){
			jugador.disminuirCont();
		}else {
			jugador.setAtacar(true);
		}
		
		if(cont%12 == 0 && jugador.getVida() > 0) {
			disminuyeVida();
		}
		
		if(cont%(300/dificultad) == 0 && enemigos.size() <= 20) {
			pos = random.nextInt(8);
			while(pos == pasado) {
				pos = random.nextInt(8);
			}
			enemigos.add(EnemigosFactory.getInstance().makeEnemigos(valorEnemigo(jugador.getVida()), pos));
			pasado = pos;
		}
		
		if(cont % 60/dificultad == 0) {
			jugador.aumentarScore();
		}
		jugador.moverPersonaje(enemigos, jugador.getVelocidad());
	}
	
	public int valorEnemigo(int vida) {
		int x = random.nextInt(105);
		int y = vida/300;
		if(x < (40 - 35*y)) {
			return 0;
		}else if (x < (70 - 55*y)) {
			return 1;
		}else if (x < (90 - 55*y)) {
			return 2;
		}else if (x < (100 - 35*y)) {
			return 3;
		}else {
			return 4;
		}
	}
	
	public void pintate(Graphics dbg) {
		jugador.pintate(dbg);
		
		copy = (LinkedList<Enemigo>) enemigos.clone();
		
		Iterator<Enemigo> iterator = copy.iterator();
		while(iterator.hasNext()) {
			Enemigo e = iterator.next();
			e.pintate(dbg);
		}
		
		dbg.setFont(new Font("Arial", Font.BOLD, 18));
		dbg.setColor(Color.white);
		String str = "Vida: " + jugador.getVida();
		dbg.drawString(str, 550, 15);
		str = "Dificultad: " + dificultad;
		dbg.drawString(str, 150, 15);
		str = "Puntaje: " + jugador.getScore();
		dbg.drawString(str, 950, 15);
	}

	public int getDificultad() {
		return dificultad;
	}

	public void setDificultad(int dificultad) {
		this.dificultad = dificultad;
	}
	
	public void setDefault() {
		jugador = new Jugador();
		cont = 0;
		enemigos.clear();
		enemigos.add(EnemigosFactory.getInstance().makeEnemigos(valorEnemigo(jugador.getVida()), random.nextInt(4)));
		enemigos.add(EnemigosFactory.getInstance().makeEnemigos(valorEnemigo(jugador.getVida()), 4 + random.nextInt(4)));
	}
	
	public Jugador getJugador() {
		return jugador;
	}
}

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

public class Personaje {
	private int px;
	private int py;
	private int vx;
	private int vy;
	private int vida;
	private int size;
	private int velocidad;
	private int golpe;
	protected BufferedImage buffered;
	protected BufferedImage[] buffers;
	protected double rotationRequired;
	protected int locationX;
	protected int locationY;
	int animation;
	
	private int cont;
	private boolean atacar;
	
	private Color color;
	
	public Personaje() {
		super();
		this.px = 50;
		this.py = 50;
		this.vx = 0;
		this.vy = 0;
		this.vida = 50;
		this.size = 30;
		
		this.cont = 20;
		this.atacar = true;
		
		this.color = new Color(100,100,100);
		
		this.buffers = new BufferedImage[4];
		for(int i=0;i<4;i++){
			String img = new StringBuilder("Personaje_").append(i).toString();
			this.buffers[i] = ImageLoader.getInstance().getImage(img);
		}
		this.buffered=this.buffers[1];
		this.rotationRequired = 0;
		this.locationX = buffers[1].getWidth() / 2;
		this.locationY = buffers[1].getHeight() / 2;
		this.animation=0;
	}
	
	public Personaje(int x, int y, int vida, int size, int vel, int golpe, Color color) {
		super();
		this.px = x;
		this.py = y;
		this.vx = 0;
		this.vy = 0;
		this.vida = vida;
		this.size = size;
		this.velocidad = vel;
		this.golpe = golpe;
		this.color = color;
		
		this.buffers = new BufferedImage[4];

		for(int i=0;i<4;i++){
			String img = new StringBuilder("Personaje_").append(i).toString();
			this.buffers[i] = ImageLoader.getInstance().getImage(img);
		}
		
		this.buffered = this.buffers[1];
		this.rotationRequired = 0;
		this.locationX = buffers[1].getWidth() / 2;
		this.locationY = buffers[1].getHeight() / 2;
		this.animation=0;
	}

	public int getPx() {
		return px;
	}

	public void setPx(int px) {
		this.px = px;
	}

	public int getPy() {
		return py;
	}

	public void setPy(int py) {
		this.py = py;
	}

	public int getVida() {
		return vida;
	}
	
	public int getVx() {
		return vx;
	}

	public void setVx(int vx) {
		this.vx = vx;
	}

	public int getVy() {
		return vy;
	}

	public void setVy(int vy) {
		this.vy = vy;
	}

	public void setVida(int vida) {
		this.vida = vida;
	}
	
	public void setSize(int size) {
		this.size = size;
	}
	
	public int getSize() {
		return size;
	}
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}
	
	public void pintate(Graphics dbg){
		AffineTransform tx = AffineTransform.getRotateInstance(rotationRequired, locationX, locationY);
		AffineTransformOp op = new AffineTransformOp(tx, AffineTransformOp.TYPE_BILINEAR);
		dbg.drawImage(op.filter(buffered, null), px, py, null);
	}
	
	public void moverX(int x) {
		if(getPx() > 10 && (getPx() + getSize()) < 1190){
			setPx(getPx() - x);
		}
		//Verificar movimiento al chocar con la ventana.
	}
	
	public void moverY(int y) {
		if(getPy() > 10 && (getPy() + getSize()) < 740){
			setPy(getPy() - y);
		}
		//Verificar movimiento al chocar con la ventana.
	}
	
	public void moverPersonaje(LinkedList<Enemigo> enemigos, int MOV){
		boolean arriba = true, abajo = true, izquierda = true, derecha = true;
		if(((getPy() + getSize()) < 730 && getPy() > 20 && (getPx() + getSize()) < 1180 && getPx() > 20) ||
		   ((getPy() + getSize()) >= 730 && getVy() < 0) || (getPy() <= 20 && getVy() > 0) ||
		   ((getPx() + getSize()) >= 1180 && getVx() < 0) || (getPx() <= 20 && getVx() > 0)){
			for(Enemigo e: enemigos) {
				if(e != this) {
					Rectangle enemigo = new Rectangle(e.getPx(), e.getPy(), e.getSize(), e.getSize());
					//Colision Arriba
					if(new Rectangle(getPx(), getPy() - MOV, getSize(), getSize() - MOV).intersects(enemigo)) {
						arriba = false;
					}
					if(new Rectangle(getPx(), getPy() + MOV, getSize(), getSize() + MOV).intersects(enemigo)) {
					//Colision Abajo
						abajo = false;
					}
					//Colision Izquierda
					if(new Rectangle(getPx() - MOV, getPy(), getSize() - MOV, getSize()).intersects(enemigo)) {
						izquierda = false;
					}
					//Colision Derecha
					if(new Rectangle(getPx() + MOV, getPy(), getSize() + MOV, getSize()).intersects(enemigo)) {
						derecha = false;
					}
				}
			}
			if(arriba && getVy() < 0) {
				setPy(getPy()+getVy());
			}
			if(abajo && getVy() > 0) {
				setPy(getPy()+getVy());
			}
			if(izquierda && getVx() < 0) {
				setPx(getPx()+getVx());
			}
			if(derecha && getVx() > 0) {
				setPx(getPx()+getVx());
			}
		}
		if(getVx() != 0 || getVy() != 0){
			if(animation >= 12) animation=0;
			if(animation % 3 == 0) buffered=buffers[animation/3];
			animation++;
			rotationRequired = Math.atan2(getVx(), getVy()*-1);
		}
	}
	
	public void modificaVida(int cambio){
		setVida(getVida() + cambio);
	}

	public boolean isAtacar() {
		return atacar;
	}

	public void setAtacar(boolean atacar) {
		this.atacar = atacar;
	}

	public int getCont() {
		return cont;
	}

	public void setCont(int cont) {
		this.cont = cont;
	}
	
	public void disminuirCont() {
		cont--;
	}

	public int getVelocidad() {
		return velocidad;
	}

	public void setVelocidad(int velocidad) {
		this.velocidad = velocidad;
	}

	public int getGolpe() {
		return golpe;
	}

	public void setGolpe(int golpe) {
		this.golpe = golpe;
	}
}

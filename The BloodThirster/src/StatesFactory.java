
public class StatesFactory {
	private static StatesFactory instance;
	
	private StatesFactory() {}
	
	public static StatesFactory getInstance() {
		if(instance == null) {
			instance = new StatesFactory();
		}
		return instance;
	}
	
	public GameState getState(String str) {
		switch(str) {
		case "Running":
			return new GameStateRunning();
		case "Paused":
			return new GameStatePaused();
		case "GameOver":
			return new GameStateGameOver();
		case "Menu":
			return new GameStateMenu();
		}
		return null;
	}
}

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class SoundLoader {
	private static SoundLoader instance;
	private Clip sencishito, letsplay, magoroberto,music;
	private AudioInputStream sencishitoStream, letsplayStream, magorobertoStream,musicStream;
	
	private SoundLoader(){
		try{
			sencishito = AudioSystem.getClip();
			sencishitoStream = AudioSystem.getAudioInputStream(new File("media/sencishito.wav"));
			sencishito.open(sencishitoStream);
			letsplay = AudioSystem.getClip();
			letsplayStream = AudioSystem.getAudioInputStream(new File("media/letsplay.wav"));
			letsplay.open(letsplayStream);
			magoroberto = AudioSystem.getClip();
			magorobertoStream = AudioSystem.getAudioInputStream(new File("media/magoroberto.wav"));
			magoroberto.open(magorobertoStream);
			music=AudioSystem.getClip();
			musicStream = AudioSystem.getAudioInputStream(new File("media/Music.wav"));
			music.open(musicStream);
		}catch(Exception e){
			System.err.println("No cargo el sonido");
		}
		music.loop(Clip.LOOP_CONTINUOUSLY);
	}
	
	public static SoundLoader getInstance(){
		if(instance == null){
			instance = new SoundLoader();
		}
		return instance;
	}
	
	public void playSound(String tag){
		switch(tag) {
		case "sencishito":
			try{
				sencishito.start();
				while(sencishito.getMicrosecondLength() != sencishito.getMicrosecondPosition()){}
				sencishito.stop();
				sencishito.setMicrosecondPosition(0);
			}catch(Exception e){
				System.err.println("No corrio el sonido");
			}
			break;
		case "letsplay":
			try{
				letsplay.start();
				while(letsplay.getMicrosecondLength() != letsplay.getMicrosecondPosition()){}
				letsplay.stop();
				letsplay.setMicrosecondPosition(0);
			}catch(Exception e){
				System.err.println("No corrio el sonido");
			}
			break;
		case "magoroberto":
			try{
				magoroberto.start();
				while(magoroberto.getMicrosecondLength() != magoroberto.getMicrosecondPosition()){}
				magoroberto.stop();
				magoroberto.setMicrosecondPosition(0);
			}catch(Exception e){
				System.err.println("No corrio el sonido");
			}
			break;
		}
	}
	public void startMusic(){
		if(music != null){
			music.loop(Clip.LOOP_CONTINUOUSLY);
		}
	}
	public void stopMusic(){
		if(music != null){
			music.stop();
		}
	}
}

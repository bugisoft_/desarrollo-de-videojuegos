public class EnemigosFactory {
	private static EnemigosFactory instance;
	
	private EnemigosFactory() {}
	
	public static EnemigosFactory getInstance() {
		if(instance == null) {
			instance = new EnemigosFactory();
		}
		return instance;
	}

	public Enemigo makeEnemigos(int selection, int pos) {
		
		switch(selection) {
		case 0:
			return new EnemigoRunner(pos);
		case 1:
			return new EnemigoRecluso(pos);
		case 2:
			return new EnemigoComun(pos);
		case 3:
			return new EnemigoHeavy(pos);
		case 4:
			return new EnemigoPoderoso(pos);
		default:
			return null;
		}
		
	}
	
}

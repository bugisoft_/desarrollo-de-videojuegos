/*
 * GameStatePaused.java
 * 
 * Copyright 2017 Edgar Daniel Fernández Rodríguez <edgar.fernandez@me.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;

public class GameStatePaused implements GameState {
	GameStateContext gc;
	
	public GameStatePaused(GameStateContext gc){
		this.gc = gc;
	}
	
	public GameStatePaused(){

	}
	
	public void setContext(GameStateContext context) {
		gc = context;
	}
	
	public void end(){
		System.out.println("No se puede dar end en pausado");
	}
	
	public void pause(){
		System.out.println("Ya esta pausado");
	}
	
	public void resume(){
		gc.setCurrent(gc.getRun());
	}
	
	public void render(Graphics g){
		g.drawImage(gc.getBackground(), 0, 0, null);
		gc.getCharacterController().pintate(g);
		g.drawImage(gc.getPauseImage(), 80, 50, null);
		g.setColor(new Color(255,255,255));
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 75));
		g.drawString("The BloodThirster", 165, 150);
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 55));
		g.drawString("Volver al Menu Principal", 165, 280);
		g.drawString("Dejar de Jugar", 165, 410);
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 70));
		g.drawString("Continuar", 165, 550);
		g.setFont(new Font("Viner Hand ITC", Font.BOLD, 35));
		g.drawString("Instrucciones", 165, 640);
		if(gc.isInstrucciones()) {
			g.drawImage(gc.getInstruccionesImage(), 120, 75, null);
		}
	}
	
	public void update(){
		
	}

	@Override
	public void keyPressed(KeyEvent e) {

	}

	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	@Override
	public void start() {
		
	}

	@Override
	public void run() {
		
	}

	@Override
	public void menu() {
		gc.setCurrent(gc.getMenu());
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if(!gc.isInstrucciones()) {
			if(e.getX() >= 165 && e.getX() <= 900 && e.getY() >= 240 && e.getY() <= 312) {
				gc.getCharacterController().setDefault();
				gc.setGameOver(true);
				menu();
			}else if(e.getX() >= 160 && e.getX() <= 670 && e.getY() >= 365 && e.getY() <= 445) {
				gc.setRunning(false);
			}else if(e.getX() >= 160 && e.getX() <= 565 && e.getY() >= 500 && e.getY() <= 565) {
				resume();
			}else if(e.getX() >= 165 && e.getX() <= 400 && e.getY() >= 612 && e.getY() <= 645) {
				gc.setInstrucciones(true);
			}
		}else if(e.getX() >= 930 && e.getX() <= 1065 && e.getY() >= 640 && e.getY() <= 665){
			gc.setInstrucciones(false);
		}
	}
}
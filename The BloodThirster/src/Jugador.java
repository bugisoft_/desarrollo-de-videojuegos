import java.awt.Color;

public class Jugador extends Personaje{
	//Espera entre ataques.
	private static final int DELAY = 15;
	//Tama�o
	private static final int SIZE = 30;
	//VelocidadMovimiento
	private static final int MOVJUGADOR= 5;
	//Golpe del jugador.
	private static final int GOLPEJUGADOR = 25;
	
	private int score;
	
	public Jugador() {
		super(585,377,150,SIZE,MOVJUGADOR,GOLPEJUGADOR,new Color(0,0,255));

		for(int i=0;i<4;i++){
			String img = new StringBuilder("Personaje_").append(i).toString();
			this.buffers[i] = ImageLoader.getInstance().getImage(img);
		}
		
		this.buffered = this.buffers[1];
		this.rotationRequired = 0;
		this.locationX = buffers[1].getWidth() / 2;
		this.locationY = buffers[1].getHeight() / 2;
		this.animation=0;
		
		setScore(0);
	}
	
	public Jugador(int x, int y, int vida) {
		super(x, y, vida, SIZE,MOVJUGADOR,GOLPEJUGADOR,new Color(0,0,255));

		for(int i=0;i<4;i++){
			String img = new StringBuilder("Personaje_").append(i).toString();
			this.buffers[i] = ImageLoader.getInstance().getImage(img);
		}
		
		setScore(0);
	}
	
	public void delayAtaque() {
		this.setCont(DELAY);
		this.setAtacar(false);
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}
	
	public void aumentarScore() {
		score++;
	}
	
	public void sound() {
		//SoundLoader.getInstance().playSound("personaje");
	}
}

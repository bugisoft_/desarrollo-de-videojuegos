import java.awt.Color;

//Caracteristicas similares al jugador
public class EnemigoComun extends Enemigo{
	//Espera entre ataques.
	private static final int DELAY = 20;
	//Velocidad de movimiento
	private static final int VELOCIDAD = 10;
	//Golpe del Enemigo
	private static final int GOLPE = 8;
	//Nombre de la imagen
	private static final String NAME = "Enemigo_";
	
	public EnemigoComun() {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),9,NAME);
	}
	
	public EnemigoComun(int pos) {
		super(50,500,200,DELAY,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
	
	public EnemigoComun(int x, int y, int vida, int delay, int pos) {
		super(x,y,vida,delay,VELOCIDAD,GOLPE,new Color(255,0,0),pos,NAME);
	}
}
